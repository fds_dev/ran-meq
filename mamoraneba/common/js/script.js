//<!-- スクロールしたら表示 -->
$(function () {
	var topBtn = $('#topGoWrap');
	topBtn.hide();
	//スクロールが100に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	});
	$(window).on('scroll load', function () {
		documentHeight = $(document).height();
		scrollUnderPos = $(window).scrollTop() + window.innerHeight;
		footerHeight = $('#footerCopy').innerHeight();
		if (documentHeight - scrollUnderPos <= footerHeight) {
			topBtn.css({
				'position': 'absolute',
				'bottom': footerHeight + 20 + 'px'
			});
		} else {
			topBtn.css({
				'position': 'fixed',
				'bottom': 20 + 'px'
			});
		}
	});
	//<!-- スクロールしたら表示 -->

	//<!-- SPメニュー -->
	var $body = $('body');
	var $btnOp = $('#btn-menu-open');
	var $btnCl = $('#btn-menu-close');
	var $menu = $('#sp-menu');
	var scrollTop;
	//メニューボタンクリックでクラスを追加
	$($btnOp).on('click', function () {
		$body.toggleClass('open-menu');
		$btnOp.hide();
		$btnCl.show();
		scrollTop = $(window).scrollTop();
		$body.addClass('modal');
		$body.css({ 'top': -scrollTop });
	});
	//メニュー以外の部分をクリックで閉じる
	$($btnCl).on('click', function () {
		$body.removeClass('open-menu');
		$btnCl.hide();
		$btnOp.show();
		$body.removeClass('modal');
		$body.css({ 'top': 0 });
		window.scrollTo(0, scrollTop);
	});
	$($menu).on('click', function () {
		$body.removeClass('open-menu');
		$btnCl.hide();
		$btnOp.show();
		$body.removeClass('modal');
		$body.css({ 'top': 0 });
		window.scrollTo(0, scrollTop);
	});
	//<!-- SPメニュー -->

	//<!-- メインビジュアル -->
	var BREAKPOINT = 768;
	var w = $(window);
	var inview = $('.js-inview');

	w.on('load scroll', function (e) {

		var scroll = w.scrollTop();
		var windowHeight = w.height();
		inview.each(function () {
			var self = $(this);
			var appearancePosition = windowHeight;
			if (e.type != 'load') {
				var ratio = self.data('view-position-ratio') || 0.6;
				appearancePosition = appearancePosition * ratio;
			}
			var position_top = self.offset().top;
			if (position_top - appearancePosition < scroll) {
				self.addClass('inview');
			}
		});
	});


	function getWindowWidth() {
		return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	}

	// アンカーリンク押下時のスクロール処理
	var sp = w.width() < BREAKPOINT;
	var spNavInner = $('.spNavInner');
	$("a[href^='#']").on('click', function (e) {
		e.preventDefault();
		var startHref = $(this).attr('href');
		var goalDom = $(startHref == "#" || startHref == "" ? 'html' : startHref);
		if (!goalDom.length || $(this).closest('#topGoInner').length) return;
		var goalPos = goalDom.offset().top;
		if (sp) {
			console.log(goalPos)
			$("html,body").animate({ scrollTop: (goalPos - (spNavInner.innerHeight() - 1)) }, 250, 'swing');
		} else {
			$("html,body").animate({ scrollTop: goalPos }, 250, 'swing');
		}
	});



});
//メインビジュアル
